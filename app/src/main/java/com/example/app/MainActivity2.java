package com.example.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        TextView welcomeTextView = findViewById(R.id.welcomeTextView);
        Button b = findViewById(R.id.ContinueButton);

        getSupportActionBar().setTitle("Welcome");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#081d5c")));

        Intent intent = getIntent();
        String username = intent.getStringExtra("username");
        boolean loggedIn = intent.getBooleanExtra("loggedIn", false);

        if (loggedIn) {
            welcomeTextView.setText("Welcome, " + username + "!");

        } else {
            Toast.makeText(getApplicationContext(), "You must be logged in to view this page.", Toast.LENGTH_SHORT).show();
            finish();
        }

b.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(MainActivity2.this, EmployeeNav.class);
        startActivity(intent);
    }
});

    }
}